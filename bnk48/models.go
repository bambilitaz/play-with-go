package bnk48

// Member is model of bnk48 member
type Member struct {
	Birthday      string   `json:"birthday"`
	BloodType     string   `json:"blood_type"`
	EngFirstname  string   `json:"english_first_name"`
	EngLastname   string   `json:"english_last_name"`
	Facebook      string   `json:"facebook"`
	Height        int      `json:"height"`
	Hobby         string   `json:"hobby"`
	Instagram     string   `json:"instagram"`
	Like          []string `json:"like"`
	Nickname      string   `json:"nickname"`
	Province      string   `json:"province"`
	ThaiFirstname string   `json:"thai_first_name"`
	ThaiLastname  string   `json:"thai_last_name"`
}
