package members

// ILogin is interface
type ILogin interface {
	Email() string
	Password() string
}

// Member is member model
type Member struct {
	ID           string
	Name         string
	EmailAddress string
	Passw        string
	Enabled      bool
}

// Copy return the copy of member
func (m *Member) Copy() *Member {
	newM := *m
	return &newM
}

// Email return EmailAddress
func (m *Member) Email() string {
	return m.EmailAddress
}

// Password return Password
func (m *Member) Password() string {
	return m.Passw
}
