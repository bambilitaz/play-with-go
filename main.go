package main

import (
	"fmt"
)

func main() {
	fmt.Println("Sunday go GO!")

	bnkSvc := NewBNKService()
	members, err := bnkSvc.GetDataMembers(NewRequester())
	if err != nil {
		fmt.Println("Error getData:", err)
		return
	}

	for _, m := range members {
		fmt.Println(fmt.Sprintf(`%s %s (%s)`, m.ThaiFirstname, m.ThaiLastname, m.Nickname))
	}
}
