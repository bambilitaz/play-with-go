package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// BNKServiceTestSuite is suite
type BNKServiceTestSuite struct {
	suite.Suite
}

// NewBNKServiceTestSuite return BNKServiceTestSuite
func NewBNKServiceTestSuite() *BNKServiceTestSuite {
	return &BNKServiceTestSuite{}
}

func TestBNKServiceTestSuite(t *testing.T) {
	suite.Run(t, NewBNKServiceTestSuite())
}

func (ts *BNKServiceTestSuite) TestGetDataMembers_GivenJSON_ExpectMembers() {

	requester := NewMockRequester()
	requester.On("Get", "https://raw.githubusercontent.com/whs/bnk48json/master/bnk48.json").Return(`
	[
		{
			"birthday": "1996-05-02",
			"blood_type": "B",
			"english_first_name": "CHERPRANG",
			"english_last_name": "AREEKUL",
			"facebook": "bnk48official.cherprang",
			"height": 160,
			"hobby": "Cosplay",
			"instagram": "cherprang.bnk48official",
			"like": [
				"White color",
				"Tofu"
			],
			"nickname": "Cherprang",
			"province": "Bangkok",
			"thai_first_name": "เฌอปราง",
			"thai_last_name": "อารีย์กุล"
		},
		{
			"birthday": "2002-06-17",
			"blood_type": "AB",
			"english_first_name": "CHRISTIN",
			"english_last_name": "LARSEN",
			"facebook": "bnk48official.namhom",
			"height": 165,
			"hobby": "Playing Guitar",
			"instagram": "namhom.bnk48official",
			"like": [
				"White color",
				"Chocolate"
			],
			"nickname": "Namhom",
			"province": "Sisaket",
			"thai_first_name": "คริสติน",
			"thai_last_name": "ลาร์เซ่น"
		}
	]`, nil)

	bnkSvc := NewBNKService()
	mems, err := bnkSvc.GetDataMembers(requester)
	is := assert.New(ts.T())
	if is.NoError(err) {
		is.Equal(2, len(mems))
		is.Equal("CHERPRANG", mems[0].EngFirstname)
		is.Equal("CHRISTIN", mems[1].EngFirstname)
	}

	requester.AssertExpectations(ts.T())
}

func (ts *BNKServiceTestSuite) TestGetDataMembers_GivenInvalidJSON_ExpectErrors() {

	requester := NewMockRequester()
	requester.On("Get",
		"https://raw.githubusercontent.com/whs/bnk48json/master/bnk48.json").
		Return(`<invalidJSON>`, nil)

	bnkSvc := NewBNKService()
	mems, err := bnkSvc.GetDataMembers(requester)
	is := assert.New(ts.T())
	if is.Nil(mems) {
		is.Equal(err.Error(), "Error JSON:invalid character '<' looking for beginning of value")
	}

	requester.AssertExpectations(ts.T())
}

func (ts *BNKServiceTestSuite) TestGetDataMembers_GivenBadRequester_ExpectErrors() {

	requester := NewMockRequester()
	requester.On("Get",
		"https://raw.githubusercontent.com/whs/bnk48json/master/bnk48.json").
		Return("", fmt.Errorf("Mock error msg"))

	bnkSvc := NewBNKService()
	mems, err := bnkSvc.GetDataMembers(requester)
	is := assert.New(ts.T())
	if is.Nil(mems) {
		is.Equal(err.Error(), "Error Request:Mock error msg")
	}

	requester.AssertExpectations(ts.T())
}
