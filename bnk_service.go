package main

import (
	"encoding/json"
	"errors"

	"bitbucket.org/bambilitaz/play-with-go/bnk48"
)

// BNKService is a a service for BNK
type BNKService struct{}

// NewBNKService return new BNKService
func NewBNKService() *BNKService {
	return &BNKService{}
}

// GetDataMembers return data members
func (bnkSvc *BNKService) GetDataMembers(request IRequester) ([]*bnk48.Member, error) {
	body, err := request.Get("https://raw.githubusercontent.com/whs/bnk48json/master/bnk48.json")
	if err != nil {
		return nil, errors.New("Error Request:" + err.Error())
	}

	members := make([]*bnk48.Member, 0)
	err = json.Unmarshal([]byte(body), &members)
	if err != nil {
		return nil, errors.New("Error JSON:" + err.Error())
	}

	return members, nil
}
